from debian:jessie

RUN echo deb http://www.deb-multimedia.org jessie main non-free >> /etc/apt/sources.list && echo deb-src http://www.deb-multimedia.org jessie main non-free >> /etc/apt/sources.list
RUN apt-get update && apt-get install --no-install-recommends -qq --force-yes deb-multimedia-keyring && apt-get clean
RUN apt-get update && apt-get install --no-install-recommends -y -qq build-essential texi2html chrpath git gawk python texinfo diffstat wget cpio git vim imagemagick python3 librsvg2-bin xsltproc gettext zip xfonts-utils ffmpeg bash && apt-get clean 
RUN rm -v /bin/sh && ln -s /bin/bash /bin/sh
COPY openvario /home/openvario/
RUN useradd -m openvario && chown -R openvario:users /home/openvario
WORKDIR /home/openvario
USER openvario
RUN chmod +x /home/openvario/*.sh
ENTRYPOINT /bin/bash
VOLUME ['/home/openvario']
